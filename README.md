# semeval2019
### Metric
[F1-score (weighted)](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html)

### Evaluator
```python
cv = StratifiedKFold(n_splits=10, random_state=0)
cross_val_score(estimator, X, y, cv=cv, scoring='f1_weighted')
```

#### Trained and tested on holdout set of 643 samples (10 folded cross validation)
| Model   |      Features      |  F1-score (weighted) |
|----------|:-------------:|------:|
| **LinearSVC**~ | **Word & Char ngrams (text)** | **0.7815**
| Logistic Regression / LinearSVC |  Word & Char ngrams (text) | 0.768 |
| RandomForest |    Word ngrams (text)   |   0.757 |
| VotingClassifier* | Word & Char ngrams (text) | 0.7532
| Gradient Boosting | Word & Char ngrams (text) | 0.7407
| **Feedforward network** | **Universal Sentence Encoder (title)** | **0.7390**
| BernoulliNB | Word & Char ngrams (text) | 0.7387
| VotingClassifier* | Word & Char ngrams (title) | 0.6798
| BernoulliNB | Word & Char ngrams (title) | 0.6706
| Gradient Boosting | Word & Char ngrams (title) | 0.6655
| Logistic Regression | Word & Char ngrams (title) | 0.6502
| NBSVM | Word & Char ngrams (text) |    0.600 |

#### Trained on training set of 1 000 000 samples and tested on holdout set of 643 samples (10 folded cross validation)
| Model   |      Features      |  F1-score (weighted) |
|----------|:-------------:|------:|
| LogisticRegression |  Word & Char ngrams (title) | 0.63 |
| X |    A   |   0 |
| Y | B |    0 |


VotingClassifier* = LogReg, LGBM, XGBoost

LinearSVC~ = [baseline_1.py](https://gitlab.com/timpal0l/semeval_2019/blob/master/models/baseline_1.py)

