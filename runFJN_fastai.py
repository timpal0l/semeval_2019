"""FOI contribution for the PAN19 hyperpartisan news detection task"""
# Version: 2018-09-24

# Parameters:
# --inputDataset=<directory>
#   Directory that contains the articles XML file with the articles for which a prediction should be made.
# --outputDir=<directory>
#   Directory to which the predictions will be written. Will be created if it does not exist.

from __future__ import division

import os
import getopt
import sys
import xml.sax
import random
from sklearn.externals import joblib

import numpy as np
import torch

from fastai.text import *
from fastai.datasets import *

random.seed(42)
runOutputFileName = "prediction.txt"


def parse_options():
    """Parses the command line options."""
    try:
        long_options = ["inputDataset=", "outputDir="]
        opts, _ = getopt.getopt(sys.argv[1:], "d:o:", long_options)
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    inputDataset = "undefined"
    outputDir = "undefined"

    for opt, arg in opts:
        if opt in ("-d", "--inputDataset"):
            inputDataset = arg
        elif opt in ("-o", "--outputDir"):
            outputDir = arg
        else:
            assert False, "Unknown option."
    if inputDataset == "undefined":
        sys.exit(
            "Input dataset, the directory that contains the articles XML file, is undefined. Use option -d or --inputDataset.")
    elif not os.path.exists(inputDataset):
        sys.exit("The input dataset folder does not exist (%s)." % inputDataset)

    if outputDir == "undefined":
        sys.exit(
            "Output path, the directory into which the predictions should be written, is undefined. Use option -o or --outputDir.")
    elif not os.path.exists(outputDir):
        os.mkdir(outputDir)

    return (inputDataset, outputDir)


########## SAX ##########

class HyperpartisanNewsRandomPredictor(xml.sax.ContentHandler):
    def __init__(self, outFile):
        xml.sax.ContentHandler.__init__(self)
        self.outFile = outFile
        self.text = ""
        self.inArticle = False
        self.articleId = 0

        self.prediction = "false"
        # Load the trained model
        filename = 'models/finalized_model.sav'
        self.loaded_model = joblib.load(filename)

        #path = "/home/frejoh/git/Timpa/semeval_2019"
        path = "/mnt/data/semeval_2019"
        data_clas = TextClasDataBunch.load(path,bs=10)
        learn = text_classifier_learner(data_clas, drop_mult=0.5)
        #learn.load_encoder('ft_text_enc')
        #learn.load_state_dict(torch.load(path/f'{ft_text_enc}.pth'))
        learn.model[0].load_state_dict(torch.load(path+"/models/ft_text_enc.pth", map_location="cpu"))
        learn.freeze()
        learn.load("partisanclassifier_text_title")
        learn.model.eval() # Needed for avoiding dropout
        self.learn = learn

    def startElement(self, name, attrs):
        if name == "article":
            self.articleId = attrs.getValue("id")  # id of the article for which hyperpartisanship should be predicted
            self.inArticle = True
            title = attrs.getValue("title")
            if len(title) > 0:
                self.title = title
            else:
                self.title = " "

    def characters(self, content):
        if self.inArticle:
            content = content.replace('\n', '')
            content = content.replace('\t', '')
            self.text += content

    def endElement(self, name):
        if name == "article":
            self.inArticle = False
            text = self.text
            self.text = ""

            #prediction = self.loaded_model.predict([text])
            seq = (text, self.title)
            stringToPredict = " ".join(seq)
            prediction = self.learn.predict(stringToPredict)

            #if prediction[0] == '0':--
            #    self.prediction = "false"
            #if prediction[0] == 0:
        #        self.prediction = "false"
            if np.argmax(prediction[2].numpy()) == 0:
                self.prediction = "false"
            else:
                self.prediction = "true"

            # confidence = random.random() # random confidence value for prediction
            # output format per line: "<article id> <prediction>[ <confidence>]"
            #   - prediction is either "true" (hyperpartisan) or "false" (not hyperpartisan)
            #   - confidence is an optional value to describe the confidence of the predictor in the prediction---the higher, the more confident
            self.outFile.write(str(self.articleId) + " " + self.prediction + "\n")


########## MAIN ##########


def main(inputDataset, outputDir):
    """Main method of this module."""

    with open(outputDir + "/" + runOutputFileName, 'w', encoding='utf-8') as outFile:
        for file in os.listdir(inputDataset):
            if file.endswith(".xml"):
                with open(inputDataset + "/" + file, encoding='utf-8') as inputRunFile:
                    xml.sax.parse(inputRunFile, HyperpartisanNewsRandomPredictor(outFile))

    print("The predictions have been written to the output folder.")


if __name__ == '__main__':
    main(*parse_options())
